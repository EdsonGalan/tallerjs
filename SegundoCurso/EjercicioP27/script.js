function function_resolve(){
  try{
    const result = divide(5,1);
    console.log(result);
  }catch(err){
    console.log(err.message);
  }
}

function function_rejected(){
  try{
    const result = divide(5,0);
    console.log(result);
  }catch(err){
    console.log(err.message);
  }
}

function divide(dividendo, divisor){
  return new Promise((resolve, reject) => {
    if( divisor === 0 ){
      reject(new Error('No se puede dividir sobre 0'));
    }else{
      resolve(dividendo / divisor);
    }
  });
}
