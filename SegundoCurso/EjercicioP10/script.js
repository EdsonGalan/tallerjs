let datos = {"delay":"5"};

let request = new XMLHttpRequest();
request.open('POST', 'https://httpbin.org/delay/5', true);

let contador = 0;
request.onreadystatechange = function(){
  enconsola(['¿Cuantas veces se revisó?: ', contador]);
  contador++;
  if(request.readyState === XMLHttpRequest.DONE && request.status ===200){
    enconsola(JSON.parse(request.responseText));
    enpantalla(request.responseText);
  }
};

document.getElementById('request')
  .addEventListener('click', function(){
    this.style.display = 'none';
    request.send();
  })
