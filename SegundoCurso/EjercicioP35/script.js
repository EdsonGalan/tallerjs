(function(){
  fetch('https://httpbin.org/post',{
    method:'POST',
    headers:{
      'Content-Type':'application/x-www-form-urlencoded'
    },
    body:'a=1&b=2'
  })
  .then(function(response){
    console.log('response: ', response);
    return response.json();
  })
  .then(function(data){
    console.log('data = ', data);
  })
  .catch(function(err){
    console.error(err);
  })
}());
