const req = new XMLHttpRequest();
//Listener para solicitudes completadas
req.onload = function(){
  //Procesa los datos de devolución
  if( req.status >= 200 && req.status < 300 ){
    // Que se tiene que hacer con los datos
    enconsola( ['success!', req] );
  }else{
    // Que hacemos cuando falla
    enconsola('Fail!');
  }
  // Este código se ejecuta de independiente de la solicitud
  enconsola('Fin de la solicitud');
}

// Se abre una nueva solicitud ASINCRONA [true]
req.open('GET', 'https://jsonplaceholder.typicode.com/posts/1', true);
req.send();
