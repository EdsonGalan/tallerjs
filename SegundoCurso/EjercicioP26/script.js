function dispararFuncion1(){
  // Ejemplo 1
  let promise1 = new Promise((resolve, reject)=>{
    // Una tarea asíncrona usando setTimeout
    setTimeout(function(){
      resolve("'Resolución correcta'");
    },2500);
  });
  promise1.then(mensaje => {
    console.log("Mensaje de solución: "+ mensaje);
  });
  console.log(promise1);
}

function dispararFuncion2(){
  // Ejemplo 2
  let promise2 = new Promise(function(resolve, reject){
    // Una tarea asíncrona usando setTimeout
    setTimeout(function(){
      reject('Done!');
      // reject(Error('Data could not be found'));
    },2500);
  })
  .then(function(e) {console.log('done',e);})
  .catch(function(e){console.log('catch: ',e);});
  console.log(promise2);
}
