// Son funciones que se pasan como parámetros de otras Funciones
// y que se ejecutan dentro de éstas

// Callbacks
function ejemplofuncion(fnParam){
  fnParam('Beca');
}

ejemplofuncion(function(param2){
  console.log(`Hola ${param2}`);
});
