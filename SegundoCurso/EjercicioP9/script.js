let req = new XMLHttpRequest(),
    method='GET',
    url='https://jsonplaceholder.typicode.com/posts/1/comments';
req.open(method, url, true);
let contador = 0;
req.onreadystatechange = function(){
  enconsola(['¿Cuantas veces se revisó?: ', contador]);
  contador++;
  // XMLHttpRequest.Done = 4
  if(req.readyState === XMLHttpRequest.DONE && req.status === 200){
    enconsola(req.responseText);
    enpantalla(req.responseText);
  }
};
// req.send();
document.getElementById('request')
  .addEventListener('click', function(){
    this.style.display = 'none';
    req.send();
  });
