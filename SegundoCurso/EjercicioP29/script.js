function function_main(){
  new Promise((resolver, reject)=>{
    console.log('Inicial');
    resolver();
  })
  .then(()=>{
    console.log('Haz eso 0');
    throw new Error('Algo falló');
    console.log('Haz esto 1');
  })
  .catch(()=>{
    console.log('Haz eso 2');
  })
  .then(()=>{
    console.log('Haz esto sin que importe lo que sucedió antes');
  })
}
