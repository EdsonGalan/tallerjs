function function_main(){
  // Encadenamiento
  new Promise(function(resolve, reject){
    // Una tarea asníncrona usando setTimeout
    setTimeout(function(){
      resolve(10);
    },250);
  })
  .then(function(num){
    console.log('First then: ', num);
    return num * 2;
  })
  .then(function(num){
    console.log('Second then: ', num);
    return num * 2;
  })
  .then(function(num){
    console.log('Last then: ', num);
  });
}
