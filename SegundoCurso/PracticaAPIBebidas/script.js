'use strict';
// Función que se invoca a sí misma
(function loadcocktails(){
  // Crea un XMLHttpRequest Object
  const req = new XMLHttpRequest();
  // Define a callback function
  req.onload = function() {
    let data = JSON.parse(req.responseText).drinks;
    console.log(data);
    let strarticle = "";
    for(let cocktail of data){
      console.log(cocktail);
      strarticle += "<article class='cocktail'>"+
                      "<h3>"+cocktail.strDrink+"</h3>"+
                      "<div class='img-content'>"+
                        "<img class='img-cocktail' alt='"+cocktail.strDrink+"' src='"+ cocktail.strDrinkThumb +"'>"+
                      "</div>"+
                      "<div class='description'>"+
                        "<p><strong>Category: </strong>"+ cocktail.strCategory +"</p>"+
                        "<p><strong>Alcoholic: </strong>"+ cocktail.strAlcoholic +"</p>"+
                      "</div>"+
                      "<div class='btn-content'>"+
                        "<button class='btn-order'>Order Now</button>"+
                      "</div>"+
                    "</article>";
    }
    console.log(strarticle);
    document.getElementById('cocktailarticles').innerHTML = strarticle;
  }
  // Send a request
  req.open("GET", "https://www.thecocktaildb.com/api/json/v1/1/search.php?s");
  req.send();
}());

// <!-- Cocktails -->
// <article class="cocktail">
//   <h3>Cocktails Name</h3>
//   <div class="img-content">
//     <img class="img-cocktail" alt="Cocktail Name" src="https://www.thecocktaildb.com/images/media/drink/vyxwut1468875960.jpg">
//   </div>
//   <div class="description">
//     <p><strong>Category: </strong>Nombre Categoría</p>
//     <p><strong>Alcoholic: </strong>Alcohol</p>
//   </div>
//   <div class="btn-content">
//     <button class="btn-order">Order Now</button>
//   </div>
// </article>
