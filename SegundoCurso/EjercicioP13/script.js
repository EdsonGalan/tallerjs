const pets = [
  {name:'lucho', especies:'dog'},
  {name:'felix', especies:'cat'},
  {name:'nemo', especies:'fish'}
];
//
// var petNames = [];
// for(let pet of pets){
//   petNames.push( pet.name );
// }
// Template String
// console.log(`Pets names >> : ${petNames}`);
// console.log(`Ejemplo template string A: ${5+5}`);
// console.log(`Ejemplo template string B: ${petNames.lenght > 3 ? "Ok": "cancel"}`);

// Arrow function
const petNames = pets.map( animal => animal.name);
// Un objeto Map puede iterar sobre sus elementos en orden de inserción.
console.log(`Pets names with map: ${petNames}`);
