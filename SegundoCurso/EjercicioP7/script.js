const req = new XMLHttpRequest();
req.addEventListener('load', () =>{
  // Procesa los datos de devolución si el estatus es 200
  if(req.status === 200){
    // Formato Texto
    enconsola(req.responseText);
    // Formato JSON
    enconsola(JSON.parse(req.responseText));
    enpantalla(req.responseText);
  }else{
    enconsola([req.responseText]);
  }
});

req.addEventListener('error', ()=>{
  enconsola('Error de red');
});
// Se abre una solicitud ASINCRONA [true]
req.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
req.send(null);
