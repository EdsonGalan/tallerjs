function errores(){
  try{
    funcion_que_no_existe();
  }catch(e){
    console.error(`Error detectado: ${e.description}`);
  }
  console.log("Continua la función aún después del error");
}

errores();
