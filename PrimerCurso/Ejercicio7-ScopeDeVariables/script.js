var a = 1;                // Variable global a
console.log(a);

function x(){
  console.log(a);         // A no existe (indefined)
  var a = 5;              // Variable a nivel función

  console.log(a);         // Imprime el valor de a (5)
  console.log(window.a);  // Imprime el valor de la variable global a (1)
}

x();
console.log(a);           // Imprime la variable global
