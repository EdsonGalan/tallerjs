var myArray = [1, 2, 3, 4];

function numMaxMin(array){
  // Obtiene en numero máximo
  let numMaximo = array[0];
  for(let num of array){
    if(numMaximo < num)
      numMaximo = num;
  }
  console.log("El número máximo es: " + numMaximo);
  // Obtiene el número mínimo
  let numMin = array[0];
  for(let num of array){
    if(numMin > num)
      numMin = num;
  }
  console.log("El número mínimo es: " +numMin);
}

numMaxMin(myArray);
