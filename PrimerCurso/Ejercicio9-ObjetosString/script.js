let texto1 = '¡Hola a todos!';
let texto2 = 'Otro texto';

let texto3 = new String('¡Hola a todos!');
let texto4 = new String('Otro mensaje');

console.log(typeof(texto1));
console.log(typeof(texto2));
console.log(texto1);
console.log(texto2);
console.log(texto1.length);
console.log(texto2.length);

console.log(typeof(texto3));
console.log(typeof(texto4));
console.log(texto3);
console.log(texto4);
console.log(texto3.length);
console.log(texto4.length);

// Métodos String()
console.log(texto1.toUpperCase());
console.log(texto2.substr(0, 5));
console.log(texto3.split(" "));
console.log(texto4.repeat(5));
