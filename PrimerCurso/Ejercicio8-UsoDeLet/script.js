// Opción 1: Bucle con let
console.log("Antes: "+ p);
for(let p=0; p<3; p++)
  console.log("-", p);

console.log("Después: ", p);
console.log("--------------------");
// Opcion 2: Bucle con var
console.log("Antes: ", p);
for(var p=0;p<3;p++)
  console.log("-", p);

console.log("Después: ", p);

try {
  const NAME = 'Texto';
  console.log(NAME);
  NAME='CambioValor';
} catch (e) {
  console.error("No se puede cambiar el valor de una const");
}

const user = {name:'Galan'};
user.name = 'Edson';
user.lastname = 'Galan';
user.age = 24;
console.log(user);
